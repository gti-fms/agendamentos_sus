Sentry.init do |config|
  config.dsn = 'https://af5b77ece31f4014b35e2070152583a9@o1219174.ingest.sentry.io/6547957'
  config.breadcrumbs_logger = [:active_support_logger, :http_logger]

  # Set traces_sample_rate to 1.0 to capture 100%
  # of transactions for performance monitoring.
  # We recommend adjusting this value in production.
  config.traces_sample_rate = 1.0
  # Alternately, you can configure Sentry to run only in certain environments by configuring the enabled_environments list.
  config.enabled_environments = %w[production staging]
end
