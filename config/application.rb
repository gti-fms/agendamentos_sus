require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
# Dotenv::Railtie.load

module MyScheduling
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.time_zone = 'America/Fortaleza'
    config.i18n.default_locale = 'pt-BR'
    config.active_record.default_timezone = :local

    ## ------ Custom Site Properties ----- ##
    config.company_title = 'Fundação Municipal de Saúde'
    config.system_title = 'Agendamentos - SUS'
    config.system_footer = "© #{Date.today.year}, Todos os direitos reservados, Desenvolvido por GTI-FMS. Versão: " + Rails.root.to_s.split(File::SEPARATOR).last
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*',
                 headers: :any,
                 expose: [
                     "X-Requested-With",
                     "Content-Type",
                     "Authorization",
                     "Accept",
                     "Client-Security-Token",
                     "Accept-Encoding",
                     "iat",
                     "exp",
                     "jti"
                 ],
                 methods: %i[get post put patch delete options head]
      end
    end
    config.middleware.use Rack::Attack
    # config.middleware.use ActionDispatch::Flash
    # config.api_only = true
  end
end
