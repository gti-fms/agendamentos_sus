Rails.application.routes.draw do
  require 'sidekiq/web'
  require 'sidekiq/cron/web'
  if Rails.env.production?
    Sidekiq::Web.use Rack::Auth::Basic do |username, password|
      ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(username),
                                                  ::Digest::SHA256.hexdigest(ENV['SIDEKIQ_USERNAME'])) &
        ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(password),
                                                    ::Digest::SHA256.hexdigest(ENV['SIDEKIQ_PASSWORD']))
    end
  end
  mount Sidekiq::Web, at: '/sidekiq'
  get 'detail_scheduling/index', to: 'detail_scheduling#index', as: :detail_scheduling_index
  get 'detail/index', to: 'detail_scheduling#detail', as: :detail_index
  get 'sus_card/index', to: 'detail_scheduling#scheduling_card_sus', as: :sus_card_index
  get 'regulations_queue', to: 'regulations_queue#index'
  get 'regulation_queue/:id', to: 'regulations_queue#show', as: :regulation_queue
  get 'procedures', to: 'regulations_queue#procedures_by_cbo'

  # get 'detail_scheduling/test'
  root 'main#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # If any other non defined route were accessed, redirect to 404 page
  match '*path', via: :all, to: redirect('/404')
end
