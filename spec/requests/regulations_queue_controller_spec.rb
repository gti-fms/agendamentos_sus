require 'rails_helper'

RSpec.describe RegulationsQueueController, type: :request do
  
  let!(:regulation) {RegulationQueue.create(regulation_id: 1, fields:{cbo_descricao: 'teste 1', cbo_codigo: 1, posicao_fila: 1,
    procedimento_solicitado: 'procedimento 1', procedimento_solicitado_codigo: 1})}

  describe "GET index" do
    it "returns a successful response" do
      get regulations_queue_path
      expect(response).to be_successful
    end

    it "renders cbo select" do
      get regulations_queue_path
      expect(response.body).to include('<option value="1">teste 1</option>')
    end

    it "lists regulations" do
      (2..5).each do |i|
        RegulationQueue.create(regulation_id: i, fields:{cbo_descricao: 'teste 1', cbo_codigo: 1, posicao_fila: i,
          procedimento_solicitado: 'procedimento 1', procedimento_solicitado_codigo: 1})
      end
      get regulations_queue_path(cbo: 1, procedure: 1)
      expect(response.body).to include('<td> 1 </td>', '<td> 2 </td>', '<td> 3 </td>','<td> 4 </td>','<td> 5 </td>' )
    end
  end

  describe "GET Show" do
    it "renders a successfull response" do
      get regulation_queue_path(1, format: 'json')
      expect(response).to be_successful
    end

    it "returns the regulation data" do
      RegulationQueue.create(regulation_id: 2, fields:{cbo_descricao: 'teste 2', cbo_codigo: 2, posicao_fila: 2,
        procedimento_solicitado: 'procedimento 2', procedimento_solicitado_codigo: 2})
      get regulation_queue_path(2, format: 'json')
      expect(response.body).to include('teste 2', 'procedimento 2')
      expect(response.body).not_to include('teste 1', 'procedimento 1')
    end
  end
end
