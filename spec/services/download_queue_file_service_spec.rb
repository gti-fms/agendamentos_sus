require 'rails_helper'
require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = "fixtures/vcr_cassettes"
  config.hook_into :webmock
end

RSpec.describe DownloadQueueFileService do

  it 'return true on success' do
    VCR.use_cassette("queue_file") do
      expect(DownloadQueueFileService.call).to be_truthy
    end
  end

  it 'return false on failure' do
    allow(Net::HTTP).to receive(:get_response).and_raise(StandardError.new('return false on failure test - log'))
    expect(DownloadQueueFileService.call).to be_falsey
  end
end
