#frozen_string_literal: true

require 'rails_helper'

RSpec.describe UpdatePostgresqlQueueService do
  let(:queue_file) {open('fixtures/queue_file/queue.json', 'r')}
  let(:changed_queue_file) {open('fixtures/queue_file/changed_queue.json', 'r')}
  let(:regulation_queue_file) { RegulationQueueFile.create(parsed: false)}
  it 'creates new regulations' do
    allow(RegulationQueueFile).to receive(:last).and_return (regulation_queue_file)
    allow(regulation_queue_file).to receive_message_chain(:file, :download) {queue_file.read}
    expect {UpdatePostgresqlQueueService.call}.to change{RegulationQueue.count}.by(5)
  end
  
  it 'updates changed regulations' do
    allow(RegulationQueueFile).to receive(:last).and_return (regulation_queue_file)
    allow(regulation_queue_file).to receive_message_chain(:file, :download) {queue_file.read}
    UpdatePostgresqlQueueService.call
    regulation_queue_file.parsed = false
    allow(regulation_queue_file).to receive_message_chain(:file, :download) {changed_queue_file.read}
    expect {UpdatePostgresqlQueueService.call}.to_not change {RegulationQueue.count}
    regulation_names = RegulationQueue.pluck(Arel.sql("fields->>'paciente_nome'"))
    expect(regulation_names).to include('Teste 1', 'Teste 2', 'Teste 3', 'Teste 4', 'Teste 5')
  end
end
