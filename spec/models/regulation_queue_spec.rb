require 'rails_helper'

RSpec.describe RegulationQueue, type: :model do
  let!(:regulation) {RegulationQueue.create(regulation_id: 1, fields:{cbo_descricao: 'teste 1', cbo_codigo: 1, posicao_fila: 1,
                                            procedimento_solicitado: 'procedimento 1', procedimento_solicitado_codigo: 1})}
  it 'lists the CBOs' do
    expect(RegulationQueue.cbos).to be_an Array
    expect(RegulationQueue.cbos).to eq [['teste 1', 1]]
  end
  
  it 'lists the procedures' do
    
    expect(RegulationQueue.procedures('1')).to be_an Array
    expect(RegulationQueue.procedures('1')).to eq [['procedimento 1', 1]]
  end

  it 'ignores regulations without position' do
    RegulationQueue.create(regulation_id: 2, fields:{cbo_descricao: 'teste 1', cbo_codigo: 1,
      procedimento_solicitado: 'procedimento 1', procedimento_solicitado_codigo: 1})
    
    expect(RegulationQueue.count).to be 1
    expect(RegulationQueue.unscoped.count).to be 2
  end

  it 'lists regulations by procedure and cbo' do
    RegulationQueue.create(regulation_id: 2, fields:{cbo_descricao: 'teste 2', cbo_codigo: 2,
      procedimento_solicitado: 'procedimento 2', procedimento_solicitado_codigo: 2, posicao_fila: 1})
    expect(RegulationQueue.list_by_cbo_and_procedure('1', '1')).to eq [RegulationQueue.find(1)]
    expect(RegulationQueue.list_by_cbo_and_procedure('2', '2')).to eq [RegulationQueue.find(2)]
  end
end
