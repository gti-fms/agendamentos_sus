FROM ruby:2.7.7

RUN mkdir -p /app
WORKDIR /app

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock

RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get update -qq && apt-get install -y zlib1g-dev build-essential libssl-dev \
    libreadline-dev libyaml-dev libxml2-dev libxslt1-dev \
    libcurl4-openssl-dev software-properties-common nodejs libffi-dev postgresql-client libpq-dev libpq5
RUN npm install --global yarn
RUN gem update --system
RUN gem install bundler -v 2.0.1
RUN bundle install

COPY package.json .
COPY yarn.lock .
RUN yarn install --check-files

COPY . /app

# Add a script to be executed every time the container starts.
COPY entrypoints/web-entrypoint.sh /usr/bin/web-entrypoint.sh
RUN chmod +x /usr/bin/web-entrypoint.sh
ENTRYPOINT ["sh", "/usr/bin/web-entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
