# frozen_string_literal: true

require 'open-uri'

# Public:
# Service to download the json file containing data from the regulations queue
class DownloadQueueFileService
  def self.call
    @url = URI.parse(ENV['GESTOR_SOLUCAO_FILA_URL'])
    RegulationQueueFile.create!(parsed: false, file: { io: @url.open, filename: 'queue.json' })
    true
  rescue StandardError => e
    Rails.logger.error(e)
    false
  end
end
