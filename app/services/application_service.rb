# frozen_string_literal: true

require 'json'

# Super class for all services
class ApplicationService
  include GestorSaudeClient

  protected

  def html_error_messages(error_code)
    case error_code
    when 401
      "{\n  \"error\": 401, \n \"message\": \"#{I18n.t('regulations_queue.html.unauthorized')}\" \n}"
    when 404
      "{\n  \"error\": 404, \n \"message\": \"#{I18n.t('regulations_queue.html.not_found')}\" \n}"
    when 422
      "{\n  \"error\": 404, \n \"message\": \"#{I18n.t('regulations_queue.html.unprocessable_entity')}\" \n}"
    when 500
      "{\n  \"error\": 404, \n \"message\": \"#{I18n.t('regulations_queue.html.internal_server_error')}\" \n}"
    else
      "{\n  \"error\": 0, \n \"message\": \"#{I18n.t('regulations_queue.html.unknown')}\" \n}"
    end
  end
end
