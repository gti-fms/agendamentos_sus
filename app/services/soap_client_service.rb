class SoapClientService
  include ERB::Util
  def initialize(params)
       #@client = Savon.client(wsdl: ENV[GESTOR_SOLUCAO_URL_WSDL])
       @operation = params[:operation] || "run_soap_server"
       @id = params[:id] || ''
       @start_date = params[:start_date] || ''
       @end_date = params[:end_date] || ''
       @start_consult = params[:start_consult] || ''
       @end_consult = params[:end_consult] || ''
       @sus_card = params[:sus_card]
  end

  def call
    resp = Savon.client(wsdl: ENV['GESTOR_SOLUCAO_URL_WSDL'],
                        namespaces: { "xmlns:ns1" => "https://fmsteresina.gestorsolucao.net.br/",
                                      "xmlns:ns2"   => "https://fmsteresina.gestorsolucao.net.br/"
                                    },
                  :env_namespace => :'SOAP-ENV',
                  log: true, log_level: :warn,
                  soap_header: headers).call(@operation.to_sym, message: filter)

  end

  def get_schedules
    # exemplo retorno {"agendamento_id"=>"2879803", "agendamento_autorizacao"=>"11801451354", "agendamento_numero_agenda"=>"2134861", "agendamento_data_cadastro"=>"2013-10-02 07:10:49", "agendamento_data_hora_atendimento"=>"2013-10-16 15:00", "paciente_nome"=>"DIEGO SANTOS PARENTE DA SILVA", "paciente_cartao_sus"=>"704807502770844", "paciente_tipo_documento"=>nil, "paciente_numero_documento"=>nil, "paciente_data_nascimento"=>Tue, 29 Apr 1986, "paciente_nome_mae"=>"SEBASTIANA SANTOS PARENTE DA SILVA", "paciente_sexo"=>"M", "paciente_endereco"=>"RUA PRIMEIRO DE MAIO", "paciente_endereco_numero"=>"2488", "paciente_endereco_complemento"=>nil, "paciente_endereco_bairro"=>"PRIMAVERA", "paciente_endereco_municipio"=>"221100 - TERESINA", "paciente_endereco_uf"=>"PI", "paciente_fone1"=>"8632217906", "paciente_fone2"=>nil, "estabelecimento_cnes"=>"2726971", "estabelecimento_nome"=>"HOSPITAL GETULIO VARGAS - HGV", "estabelecimento_endereco"=>"AV FREI SERAFIM                         , 2352", "estabelecimento_endereco_bairro"=>"CENTRO", "estabelecimento_endereco_municipio"=>"221100 - TERESINA", "estabelecimento_endereco_uf"=>"PI", "estabelecimento_fone1"=>"8632213040", "estabelecimento_fone2"=>"8632216962", "profissional_cpf"=>"83544704315", "profissional_nome"=>"JAMERSON MOREIRA DE LEMOS JUNIOR", "cbo_codigo"=>"101100", "cbo_descricao"=>"MEDICO ORTOPEDISTA E TRAUMATOLOGISTA - QUADRIL", "procedimento_codigo_1"=>"0301010072", "procedimento_nome_1"=>"CONSULTA MEDICA EM ATENCAO ESPECIALIZADA", "procedimento_quantidade_1"=>"1"}

    parser = Nori.new
    if parser.parse(call.body[:server_xml_response]).present? || parser.parse(call.body[:server_xml_response]).first&.positive?
    parser.parse(call.body[:server_xml_response])['dados']['regulacoes']
    else
      parser.parse("Nenhum registro encontrado. Verifique novamente se o ID informado esta correto, caso contrário vá a uma unidade básica de saúde para maiores informações.")
    end
  end

  def send_params
    {
      runSoapServerRequest: "string",
      autenticaHeader: { usuario: ENV['GESTOR_SOLUCAO_LOGIN'], senha: ENV['GESTOR_SOLUCAO_PASSWD'] },
      acaoHeader: "gestorservermarcacao/runservice/regulacoes"
   }
  end

  def headers
    {
      :'ns2:autenticaHeader' =>  { usuario: 'FMSWEBSERVICE', senha: "WSconsultasFMS0803" },
      :'ns2:acaoHeader' => {acao: "gestorservermarcacao/runservice/regulacoes"}
    }
  end

  def filter
    file = '<dados>
             <filtro_solicitacao_regulacao>
                 <id>schedule_id</id>
                 <situacao></situacao>
                 <paciente_cartao_sus>sus_card</paciente_cartao_sus>
                 <paciente_nome></paciente_nome>
                 <estabelecimento_solicitante_cnes></estabelecimento_solicitante_cnes>
                 <estabelecimento_solicitante_nome></estabelecimento_solicitante_nome>
                 <estabelecimento_executante_cnes></estabelecimento_executante_cnes>
                 <estabelecimento_executante_nome></estabelecimento_executante_nome>
                 <procedimento_codigo></procedimento_codigo>
                 <procedimento_nome></procedimento_nome>
                 <cbo_codigo></cbo_codigo>
                 <cbo_descricao></cbo_descricao>
                 <data_cadastro_inicio>start_date</data_cadastro_inicio>
                 <data_cadastro_fim>end_date</data_cadastro_fim>
                 <data_resposta_inicio></data_resposta_inicio>
                 <data_resposta_fim></data_resposta_fim>
             </filtro_solicitacao_regulacao>
         </dados>'

    file.gsub!('schedule_id', @id.to_s)
    file.gsub!('start_date', @start_date)
    file.gsub!('end_date', @end_date)
    file.gsub!('start_consult', @start_consult)
    file.gsub!('end_consult', @end_consult)
    file.gsub!('sus_card', @sus_card.to_s)
    h(file)
  end

end
