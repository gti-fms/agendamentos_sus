# frozen_string_literal: true

# Get Patient informations form Gestor Saúde
class PatientService < ApplicationService
  def find_patient(regulation_id)
    return if regulation_id.blank?

    response = api_request(regulation_id)
    if response.is_a?(RestClient::Response) && response.code.eql?(200)
      JSON.parse(response.body)
    elsif (100..599).include?(response.http_code)
      JSON.parse(html_error_messages(response.http_code))
    end
  end
end
