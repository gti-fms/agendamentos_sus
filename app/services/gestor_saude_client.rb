# frozen_string_literal: true

require 'rest-client'

# Provide conection for Gestor Saúde services
module GestorSaudeClient
  mattr_accessor :api_request

  def api_request(regulation_id)
    RestClient::Request.execute(method: :post, url: "#{ENV['GESTOR_SAUDE_API_URL']}",
                                content_type: "application/x-www-form-urlencoded",
                                accept: "application/json",
                                payload: {'regulacao_id' => regulation_id },
                                headers: { 'Authorization' => "Bearer #{ENV['GESTOR_SAUDE_API_BEARER']}" })
  rescue RestClient::ExceptionWithResponse => e
    e
  end
end
