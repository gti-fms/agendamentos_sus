# frozen_string_literal: true

class UpdatePostgresqlQueueService
  def self.call
    regulation_queue_file = RegulationQueueFile.last
    return if regulation_queue_file.parsed?

    queue_file = regulation_queue_file.file.download
    json = JSON.parse(queue_file)
    RegulationQueueFile.transaction do
      RegulationQueue.delete_all
      json['dados'].each do |regulation|
        new_regulation = RegulationQueue.unscoped.find_or_initialize_by(regulation_id: regulation['regulacao_id'])
        if new_regulation.fields != regulation
          new_regulation.fields = regulation
          new_regulation.save
        end
      end
      regulation_queue_file.update(parsed: true)
      ActiveStorage::Attachment.all.each { |attachment| attachment.purge }
    end
  end
end
