document.addEventListener('turbolinks:load', function(event) {
    if (typeof gtag === 'function') {
        gtag('config', 'UA-141617894-1', {
            'page_location': event.data.url
        })
    }
})