$(document).ready(function () {
  $('#cbo_select').change(handle_cbo_change)
});

// callback to handle cbo changes.
// Requests the list of procedures associated with selected cbo
// and updates the procedures select with the result
function handle_cbo_change(e) {
  fetch(`procedures?cbo_code=${e.target.value}`)
    .then((result) => result.json())
    .then(function (data) {
      update_procedure_select_options(data)
    })
}

// create new options for each entry in data and append to procedures select
function update_procedure_select_options(data) {
  $('#procedure_select').empty();
  for (item of data) {
    $('#procedure_select').append(`<option value='${item[1]}'>${item[0]}</option>"`);
  }
}

function update_regulation_queue_more_field_modal(data) {
  document.getElementById('reason_for_order_change').textContent = data["fields"]["razao_para_mudanca"]
  document.getElementById('date_of_birth').textContent = data["fields"]["paciente_data_nascimento"]
  document.getElementById('expected_service_date').textContent = data["fields"]["previsao_fila_agendamento"]
  document.getElementById('registering_health_unit').textContent = data["fields"]["estabelecimento_solicitante_nome"]
  document.getElementById('sus_number').textContent = data["fields"]["paciente_cartao_sus"]
  document.getElementById('request_date').textContent = new Date(data["fields"]["data_entrada_fila"]).toLocaleDateString();
}

function fetch_regulation_queue_more_field_data(regulation_id) {
  fetch(`regulation_queue/${regulation_id}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
    },
  })
    .then((result) => result.json())
    .then(function (data) {
      update_regulation_queue_more_field_modal(data)
    })
}
