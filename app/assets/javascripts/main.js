// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function() {
        $('.footer_ass').hide();
        //$('#divView').show();

  // Smooth scrolling
  $(".js-scroll-trigger").click(function() {
    var t = $(this).attr("href");
    $('.active').removeClass('active');
    $("html, body").animate({
      scrollTop: $(t).offset().top - 50
    }, {
      duration: 1e3,
    });

    $('body').scrollspy({ target: '#main-navbar',offset: $(t).offset().top });
    $(this).parent().addClass('active');

    return false;
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#main-navbar',
    offset: 56
  });
});
