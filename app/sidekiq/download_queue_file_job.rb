# frozen_string_literal: true

class DownloadQueueFileJob
  include Sidekiq::Job

  def perform(*_args)
    DownloadQueueFileService.call
  end
end
