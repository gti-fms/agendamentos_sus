# frozen_string_literal: true

class UpdatePostgresqlQueueJob
  include Sidekiq::Job

  def perform(*_args)
    UpdatePostgresqlQueueService.call
  end
end
