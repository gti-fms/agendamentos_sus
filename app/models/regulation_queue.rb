class RegulationQueue < ApplicationRecord

  self.primary_key = :regulation_id
  self.table_name = :regulations_queue

  # Filtra apenas regulação que tenham posição na fila e ordena pela posição como integer.
  # As regras repassadas para como uma regulação obtêm posição na fila foram:
  # - Regulações com situação Agendada, Vencida e Pendente.
  # - Regulações com situação Em análise, mas que tem mais de uma crítica de agendamento.
  # - Regulações com data_entrada igual a hoje. (a não ser que a rotina de ordenação da fila tenha sido executada manualmente)
  # * crítica de agendamento : Ex.: 
  #   - Sexo incompatível com o procedimento
  #   - Idade incompatível com o procedimento
  #   - Procedimento com demanda reprimida
  # Entretanto, as críticas de agendamento não estão disponíveis e haviam outros casos de situações com posição nula fora
  # dos citados anteriormente. Por isso é realizado o filtro por posicao fila não nula.
  default_scope {where(Arel.sql("fields->>'posicao_fila' != ''")).order(Arel.sql("cast(fields->>'posicao_fila' as integer)  ASC"))}

  # Lista de CBOs com regulações na fila
  # Retorna um Array de [descrição, código] de CBOs
  def self.cbos
    pluck(Arel.sql("fields->'cbo_descricao', fields->'cbo_codigo'")).uniq
  end

  # Lista de procedimentos de um determinado CBO com regulações na fila 
  # Retorna um Array de [nome_procedimento, código_procedimento]
  def self.procedures(cbo_code)
    where("fields->>'cbo_codigo' = ?", cbo_code).pluck(Arel.sql("fields->'procedimento_solicitado', fields->'procedimento_solicitado_codigo'")).uniq
  end

  # Lista de Regulacoes por CBO e Procedimento
  def self.list_by_cbo_and_procedure(cbo, procedure)
    return none unless cbo.present? && procedure.present?

    query = "fields->>'cbo_codigo' = '#{cbo}' and fields->>'procedimento_solicitado_codigo' = '#{procedure}'"
    where(Arel.sql(query))
    .order(Arel.sql("fields->'cbo_descricao', fields->'procedimento_solicitado', fields->'posicao_fila', fields->'data_entrada_fila'"))
  end

  def request_date
    Date.parse(fields["data_entrada_fila"]) if fields["data_entrada_fila"].present?
  end

  def inclusion_date
    Date.parse(fields["data_entrada_fila"]) if fields["data_entrada_fila"].present?
  end
end
