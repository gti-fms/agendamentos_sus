class DetailSchedulingController < ApplicationController
  def index
    @app = App.new
    if(params[:number_id].present? && params[:number_id].to_i != 0)
      data = PatientService.new.find_patient(params[:number_id])
      @patient_data = data['dados'].present? ? data['dados'][0] : nil
    else
      @patient_data = 'Preencha o número de ID ou da solicitação de regulação localizado no seu comprovante de agendamento.'
    end
  end

  def detail
    if(params[:number_id].present? && params[:number_id].to_i != 0)
      data = PatientService.new.find_patient(params[:number_id])
      @patient_data = data['dados'].present? ? data['dados'][0] : nil
    else
      @patient_data = 'Preencha o número de ID ou da solicitação de regulação localizado no seu comprovante de agendamento.'
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @patient_data }
    end
  end
  def scheduling_card_sus
    if(params[:number_card].present? && params[:number_card].to_i != 0)
      @patient_data = SoapClientService.new(sus_card: params[:number_card]).get_schedules
    else
      @patient_data = 'Preencha o número do cartão SUS usado para o agendamento.'
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @patient_data }
    end
  end
end
