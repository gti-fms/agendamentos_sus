include Pagy::Backend
class RegulationsQueueController < ApplicationController
   
  def index
    cbo = params['cbo'] || ''
    procedure = params['procedure'] || ''
    list = RegulationQueue.list_by_cbo_and_procedure(cbo, procedure)
    @update_date = RegulationQueueFile.where(parsed: true).last.try(:updated_at)
    @pagy, @regulations_queue = pagy(list, link_extra: 'data-remote="true"')
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    respond_to do |format|
      format.json { render json: RegulationQueue.find_by_regulation_id(params['id']) }
    end
  end

  def procedures_by_cbo
    respond_to do |format|
      format.json {render json: RegulationQueue.procedures(params['cbo_code'])}
    end
  end
end
