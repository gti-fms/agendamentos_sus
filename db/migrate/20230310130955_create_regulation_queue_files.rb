class CreateRegulationQueueFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :regulation_queue_files do |t|
      t.boolean :parsed

      t.timestamps
    end
  end
end
