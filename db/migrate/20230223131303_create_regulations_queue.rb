class CreateRegulationsQueue < ActiveRecord::Migration[5.2]
  def change
    create_table :regulations_queue, id: false, primary_key: :regulation_id do |t|
      t.string :regulation_id
      t.jsonb :fields
      t.timestamps

      t.index :regulation_id, unique: true
    end
  end
end
