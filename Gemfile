source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.7'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.2'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Terser as compressor for JavaScript assets
gem 'terser', '>= 1.1.14'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'mini_racer', platforms: :ruby

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

# fast pagination gem
gem 'pagy', '~> 6.0'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.16.0', require: false

# SOAP client
gem 'savon', '~> 2.14'

# A simple HTTP and REST client for Ruby, inspired by the Sinatra microframework style of specifying actions: get, put, post, delete.
gem 'rest-client', '~> 2.1'

# A set of common locale data and translations to internationalize and/or localize your Rails applications.
gem 'rails-i18n', '~> 5.1', '>= 5.1.3'

# Kaminari is a Scope & Engine based, clean, powerful, agnostic, customizable and sophisticated paginator for Rails 4+
gem 'kaminari', '~> 1.1', '>= 1.1.1'

# Draper adds an object-oriented layer of presentation logic to your Rails apps.
gem 'draper', '~> 3.0', '>= 3.0.1'

# Chosen is a javascript library of select box enhancer for jQuery and Protoype. This gem integrates Chosen with Rails asset pipeline for easy of use.
gem 'chosen-rails', '~> 1.8', '>= 1.8.7'

# This gem provides jQuery and the jQuery-ujs driver for your Rails 4+ application.
gem 'jquery-rails', '~> 4.3', '>= 4.3.3'

# Track changes to your models, for auditing or versioning. See how a model looked at any stage in its lifecycle, revert it to any version, or restore it after it has been destroyed.
gem 'paper_trail', '~> 10.2'

# The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web. http://getbootstrap.com
gem 'bootstrap', '~> 4.2', '>= 4.2.1'

# Sprockets Rails integration
gem 'sprockets-rails', '~> 3.2', '>= 3.2.1'

# A rails helper that makes including svg Octicons simple.
gem 'octicons_helper', '~> 8.4', '>= 8.4.2'
gem 'rack-cors'
gem 'rack-attack'

# Helpers for the reCAPTCHA API
gem 'recaptcha', '~> 4.13', '>= 4.13.1'

# Fast mime detection by extension or content (Uses freedesktop.org.xml shared-mime-info database)
gem 'mimemagic', '~> 0.3.10'

# A gem that provides a client interface for the Sentry error logger
gem 'sentry-ruby', '~> 5.3'
# A gem that provides Rails integration for the Sentry error logger
gem 'sentry-rails', '~> 5.3'

# Simple, efficient background processing for Ruby.
gem 'sidekiq', '~> 6.5'
# A scheduling add-on for Sidekiq
gem "sidekiq-cron", "~> 1.9"

# A Ruby client to use Redis' API 
gem 'redis', '~> 4.5'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  # rspec-rails is a testing framework for Rails 3+.
  gem 'rspec-rails', '~> 3.8', '>= 3.8.2'
  # Next Generation Puma tasks for Mina
  gem 'mina-ng-puma', '~> 1.4', require: false
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # Blazing fast application deployment tool.
  gem 'mina', '~> 1.2', '>= 1.2.3'

  # Use Pry as your rails console
  gem 'pry-rails', '~> 0.3.9'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver', '~> 4.8', '>= 4.8.1'

  # Record your test suite's HTTP interactions and replay them during future test runs for fast, deterministic, accurate tests.
  gem 'vcr', '~> 6.0'

  # Library for stubbing and setting expectations on HTTP requests in Ruby.
  gem 'webmock', '~>3.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
#gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

