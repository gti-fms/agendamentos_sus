# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
2.5.3

* System dependencies
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn libjs-jquery libpq-dev libfontconfig1 libxrender1

* Configuration
Create hidden files
-------------------
.env
.ruby-version

Install ruby version
--------------------
rbenv install 2.4.1
gem install bundler:2.0.1
bundler install

Prevines error in production (find_spec_for_exe: can't find gem bundler (>= 0.a))
---------------------------------------------------------------------------------
sudo gem update --system

* Database creation
PostgreSQL
----------
sudo -u postgres psql
create role <app_name> with createdb login password '<app_password>';
\q
sudo service postgresql restart

rails db:create

* Database initialization
rails db:migrate
rails db:seed

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)
PostgreSQL - version 10

* Deployment instructions
mina setup (first time for production)
mina deploy

Development Master key = d052e1b47c0d4aca1d5268f8e9e6768f
